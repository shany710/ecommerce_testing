package tests;

import org.testng.annotations.Test;

import configuracion.CommonDriver;
import pages.CommonBase;
import pages.ListProductsPage;
import pages.RegisterPage;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

public class ListProductsTest {
	
	CommonDriver common;
	CommonBase commonBase;
	ListProductsPage listaProductos;

	@BeforeMethod
	public void beforeMethod() {
		common = new CommonDriver("chrome");
		common.getUrl("http://automationpractice.com/index.php");
		common.maximizarNavegador();
		commonBase = new CommonBase(common.getDriver(), common.getEspera());
		listaProductos = new ListProductsPage(common.getDriver(), common.getEspera());
	}

	@Test (priority=1, description="Se verifica que se puedan acceder a los links de las categorias hijas de la categoria principal DRESSES")
	public void navegarxCategoriasHijasDresses() {
		
		listaProductos.moverCursorCategoriaPrincipal(listaProductos.getCategoryDresses());
		String combTeclas = Keys.chord(Keys.CONTROL, Keys.RETURN);
		listaProductos.abrirCatHijas(combTeclas, listaProductos.getCategoryChildDresse());
		ArrayList listadoCategorias =listaProductos.navegarCategorias();
		commonBase.ordenarLista(listadoCategorias);
		System.out.println("Los elementos de la lista de navegación de categorías son: " + listadoCategorias);
		Assert.assertEquals(listadoCategorias, Arrays.asList("Casual Dresses - My Store", "Evening Dresses - My Store", "My Store", "Summer Dresses - My Store"));	
	}
	
	@Test (priority=1, description="Se verifica que se puedan acceder a los links de las categorias hijas de la categoria principal WOMEN")
	public void navegarxCategoriasHijasWomen() {
		
		listaProductos.moverCursorCategoriaPrincipal(listaProductos.getCategoryWomen());
		String combTeclas = Keys.chord(Keys.CONTROL, Keys.RETURN);
		listaProductos.abrirCatHijas(combTeclas, listaProductos.getCategoriesdWomen());
		listaProductos.abrirCatHijas(combTeclas, listaProductos.getCategoriesChildWomen());
		ArrayList listadoCategorias =listaProductos.navegarCategorias();
		commonBase.ordenarLista(listadoCategorias);
		Assert.assertEquals(listadoCategorias, Arrays.asList("Blouses - My Store","Casual Dresses - My Store",
															"Dresses - My Store","Evening Dresses - My Store",
															"My Store","Summer Dresses - My Store",
															"T-shirts - My Store", "Tops - My Store"));	
		
	}
	
	@Test (priority=1, description="Se verifica que se pueda navegar por las categorias padres del menú del listado de categorías")
	public void navegarXCategoriasPrincipales() {
	
		ArrayList<String> listaUrls= new ArrayList<String>();
		String combTeclas = Keys.chord(Keys.CONTROL, Keys.RETURN);
		listaProductos.abrirCategoriaPrincipal(listaProductos.getCategoryWomen(),combTeclas);
		// abrir categoria principal DRESSES
		listaProductos.abrirCategoriaPrincipal(listaProductos.getCategoryDresses(), combTeclas);
		// abrir categoria principal TSHRITS
		listaProductos.abrirCategoriaPrincipal(listaProductos.getCategoryTshirts(), combTeclas);
		ArrayList<String> listaCategorias = listaProductos.navegarCategorias();
		listaProductos.ordenarLista(listaCategorias);
		System.out.println("Lista de categorias pricipales:"+ listaCategorias);
		Assert.assertEquals(listaCategorias, Arrays.asList("Dresses - My Store","My Store","T-shirts - My Store","Women - My Store"));
		
	}
		
	@Test (priority=0, description="Verificar los nombre y precios de los productos presentados en una categoria sean correctos")
	public void verificarNombrePrecioProductosEnListado() {
		
		String cantProductsCategory = listaProductos.darClicCategoria(listaProductos.getCategoryWomen());
		Assert.assertEquals(cantProductsCategory, "There are 7 products.");
		ArrayList<String> items= listaProductos.verNombreProductoEnListado(listaProductos.getNombresProductos(), "title");
		listaProductos.ordenarLista(items);
		System.out.println("Lista actual nombres items\n"+items+"\n"+ "Lista esperada\n"+ Arrays.asList("Blouse","Faded Short Sleeve T-shirts", "Printed Chiffon Dress","Printed Dress", "Printed Dress", "Printed Summer Dress","Printed Summer Dress") );
		Assert.assertEquals(items, Arrays.asList("Blouse","Faded Short Sleeve T-shirts", 
												"Printed Chiffon Dress","Printed Dress",
												"Printed Dress", "Printed Summer Dress","Printed Summer Dress"));
		
		ArrayList<String> precios = listaProductos.verPrecioProductoEnListado(listaProductos.getPrecioProductosCategoria());
		listaProductos.ordenarLista(precios);
		System.out.println("Lista precios:"+precios);
		Assert.assertEquals(precios, Arrays.asList("$16.40","$16.51", "$26.00", "$27.00", "$28.98","$30.50","$50.99"));
		
	}
	
	
	@Test (priority=3, description="Verificar que se pueda navegar por la vista en miniatura de los productos presentados en una categoria")
	public void verificarVistaMiniaturaItemsEnListado(){
		listaProductos.darClicCategoria(listaProductos.getCategoryWomen());
		ArrayList<String> lista = listaProductos.verVistaMiniaturaItem(listaProductos.getImagenesProductos(), 
																		listaProductos.getBtnVerVistaMiniatura(), 
																		listaProductos.getNombreItemVistaMiniatura(), 
																		listaProductos.getBtnCerrarVistaMiniatura());
		System.out.println("Lista vista miniatura:"+lista );
		listaProductos.ordenarLista(lista);
		Assert.assertEquals(lista, Arrays.asList("Blouse","Faded Short Sleeve T-shirts", 
				"Printed Chiffon Dress","Printed Dress",
				"Printed Dress", "Printed Summer Dress","Printed Summer Dress"));
	}
	
	@Test (priority=0, description="Verificar que se pueda agregar los items presentados en una categoría a la canasta de compra")
	public void agregarItemsACanasta() {
		listaProductos.darClicCategoria(listaProductos.getCategoryWomen());
		List<String> precios = Arrays.asList("$16.51","$27.00","$26.00","$50.99","$28.98","$30.50","$16.40");
		ArrayList<String> lista = listaProductos.agregarItem("Product successfully added",precios);
		System.out.println(lista);
		//comparar la cantidad agregada y comparar el precio final sección izquierda.
	}
	
		
	@AfterMethod
	public void afterMethod() {
		
		common.quitBrowser();
	}

}
