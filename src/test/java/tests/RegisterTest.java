package tests;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.google.common.base.CaseFormat;

import configuracion.CommonDriver;
import pages.CommonBase;
import pages.RegisterPage;

public class RegisterTest {
	
	CommonDriver common;
	CommonBase commonBase;
	RegisterPage register;
	
	@BeforeMethod
	public void setup() {
		
		common = new CommonDriver("chrome");
		common.getUrl("http://automationpractice.com/index.php");
		common.maximizarNavegador();
		commonBase = new CommonBase(common.getDriver(), common.getEspera());
		register = new RegisterPage(common.getDriver(), common.getEspera());
		
		
	}
	
	@Test (priority=1, description="registrarse ingresando un correo nuevo y diligenciar los campos del formulario")
	public void completarRegistro1() {
			
			register.enterFormRegister();
			register.fillFormRegister("pruebas001@gmail.com");
			register.clicCreateAccount();
			register.selectTitle("value", "2");
			register.setFirstName("Sharon Stephany");
			register.setLastName("Rojas");
			register.setEmail("prueba.001@gmail.com");
			register.setPassword("Pruebas2021");
			register.setDayBirth("7");
			register.setMonthBirth("7");
			register.setYearBirth("1996");
			register.setCompany("Ninguna");
			register.setAddress("Calle 44 norte # 3 a 90");
			register.setCity("CALIFORNIA");
			register.setState("5");
			register.setCodePostal("99900");
			register.setCountry("21");
			register.setPhoneNumber("3345658");
			register.completeRegister();
			System.out.println(commonBase.getDirectorioEvidencias()+"test1_registerPage.png");
		//tomar captura de evidencia	
			register.takeScreenShot("test1_registerPage.png");
			Assert.assertEquals(register.displayedLogout(), true);
	}
	
	@Test (priority=1, description="registrarse ingresando un correo ya registrados")
	public void completarRegistro2() {
		
		register.enterFormRegister();
		register.fillFormRegister("cprueba665@gmail.com");
		register.clicCreateAccount();
		register.takeScreenShot("test2_registePage.png");
		Assert.assertEquals(register.getErrorEmail(),"An account using this email address has already been registered. Please enter a valid password or request a new one.");
	}
	
	@Test (priority=2, description="registrarse ingresando un correo inválido")
	public void completarRegistro3() {
		
		register.enterFormRegister();
		register.fillFormRegister("prueba.com");
		register.clicCreateAccount();
		register.takeScreenShot("test3_registerPage.png");
		Assert.assertEquals(register.getErrorEmail(), "Invalid email address.");
	}
	
	@Test (priority=3, description="registrarse ingresando un correo nuevo y diligenciar los campos obligatorios del formulario")
	public void completarRegistro4() {
		register.enterFormRegister();
		register.fillFormRegister("sharon.rojas@gmail.com");
		register.clicCreateAccount();
		register.setFieldRequired("Sharon", "MARTINEZ","sharon001", "sharon.04@gmail.com", "clle 54 ", "colorado","22", "3145560909","00099", "empresa");
		register.takeScreenShot("test4_registerPage.png");
		Assert.assertEquals(register.displayedLogout(), true);
		
	}
	
	@Test (priority=0)
	public void completarRegistro5() {
		register.enterFormRegister();
		register.fillFormRegister("sharon.5@gmail.com");
		register.clicCreateAccount();
		register.completeRegister();
		register.takeScreenShot("test5_registerPage.png");
		Assert.assertEquals(register.getErrorFieldRequired(), "There are 8 errors");
	}
		
	@AfterMethod
	public void closeBrowser() {
		common.quitBrowser();
	}
}
