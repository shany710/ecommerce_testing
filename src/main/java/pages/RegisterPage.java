package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.xml.sax.Locator;

public class RegisterPage extends CommonBase {

	// LOCATORS PAGE

	private By btnRegistrarse = By.xpath("//a[contains(@class,'login')]");
	private By email = By.name("email_create");
	private By btnCrearCuenta = By.id("SubmitCreate");
	private By firstName = By.name("customer_firstname");
	private By lastName = By.name("customer_lastname");
	private By cmpEmail = By.name("email");
	private By clave = By.name("passwd");
	private By dayBirth = By.id("days");
	private By monthBirth = By.id("months");
	private By yearBirth = By.id("years");
	private By company = By.name("company");
	private By address = By.name("address1");
	private By city = By.name("city");
	private By state = By.id("id_state");
	private By postalCode = By.name("postcode");
	private By country = By.id("id_country");
	private By mobilePhone = By.name("phone_mobile");
	private By alias = By.name("alias");
	private By btnRegister = By.id("submitAccount");
	private By btnLogOut = By.xpath("//a[@class='logout']");
	private By msgErrorEmail = By.xpath("//div[@id='create_account_error']/ol/li");
	private By modalErrorFieldRequired = By.xpath("//div[@class='alert alert-danger']/p");
	private By cmpsRequired = By.xpath("//*[contains(@class,'required form-group')]/input");
	private By title = By.name("id_gender");


	/**
	 * @param driver
	 * @param wait
	 * @param navegador
	 */
	public RegisterPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
	}

	// METHODS ACTIONS PAGE

	public void enterFormRegister() {
		darClic(btnRegistrarse);
	}

	public void fillFormRegister(String email) {
		escribir(this.email, email);
	}

	public void clicCreateAccount() {
		darClic(btnCrearCuenta);
	}

	public void setFirstName(String name) {
		escribir(this.firstName, name);
	}

	public void setLastName(String lastname) {
		escribir(this.lastName, lastname);
	}

	public void setEmail(String email) {

		WebElement emailActual = getElemento(cmpEmail);
		if (emailActual.getAttribute("value").isEmpty()) {
			escribir(this.cmpEmail, email);
		}
	}

	public void setPassword(String clave) {
		escribir(this.clave, clave);
	}

	public void setDayBirth(String opcion) {
		WebElement dayBirth = getElemento(this.dayBirth);
		selectOption(dayBirth, opcion);
	}

	public void setMonthBirth(String opcion) {
		WebElement monthBirth = getElemento(this.monthBirth);
		selectOption(monthBirth, opcion);
	}

	public void setYearBirth(String opcion) {
		WebElement yearBirth = getElemento(this.yearBirth);
		selectOption(yearBirth, opcion);
	}

	public void setCompany(String company) {
		escribir(this.company, company);
	}

	public void setAddress(String address) {
		escribir(this.address, address);
	}

	public void setCity(String city) {
		escribir(this.city, city);
	}

	public void setState(String state) {
		WebElement stateElement = getElemento(this.state);
		selectOption(stateElement, state);
	}

	public void setCodePostal(String postalCode) {
		escribir(this.postalCode, postalCode);
	}

	public void setCountry(String country) {
		WebElement countryElement = getElemento(this.country);
		selectOption(countryElement, country);
	}

	public void setPhoneNumber(String number) {
		escribir(this.mobilePhone, number);
	}

	public void setAlias(String alias) {
		escribir(this.alias, alias);
	}

	public void completeRegister() {
		darClic(btnRegister);
	}

	public boolean displayedLogout() {
		System.out.println(isDisplayed(btnLogOut));
		return isDisplayed(btnLogOut);
	}

	public String getErrorEmail() {
		return getTexto(msgErrorEmail);
	}
	public String getErrorFieldRequired() {
		return getTexto(modalErrorFieldRequired);
	}
	public void setFieldRequired(String nombre, 
			String apellido,
			String clave, 
			String email, 
			String direccion, 
			String ciudad,
			String estado, 
			String celular,
			String postCode,
			String alias ) {
//		List<WebElement> camposRequeridos = getElements(cmpsRequired);	
			setFirstName(nombre);
			setLastName(apellido);
			setPassword(clave);
			setEmail(email);
			setAddress(direccion);
			setCity(ciudad);
			setState(estado);
			setPhoneNumber(celular);
			setCodePostal(postCode);
			setAlias(alias);
			completeRegister();		
	}

	public void selectTitle(String atributo, String opcion) {

		List<WebElement> titlulos = getElements(title);
		
		for (WebElement titulo : titlulos) {
			
			if(titulo.getAttribute(atributo).equalsIgnoreCase(opcion)){
				
				darClicElement(titulo);
			}
		}
	}

}
