package pages;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import configuracion.CommonDriver;

public class CommonBase{

	private WebDriver driver;
	private WebDriverWait wait;
	private String directorioEvidencias;
	
	public CommonBase(WebDriver driver,WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		directorioEvidencias = System.getProperty("user.dir")+"\\evidencias\\";
		
	}

	public String getDirectorioEvidencias() {
		return directorioEvidencias;
	}

	public void darClic(By locator) {
		
		wait.until(ExpectedConditions.elementToBeClickable(locator)).click();
	}
	
	public void darClicElement(WebElement element) {
		element.click();
	}
	
	public void mouseOver(By locator) {
		
		WebElement elemento = driver.findElement(locator);
		Actions action = new Actions(driver);
		action.moveToElement(elemento).build().perform();
	}
	
	public void mouseOverElement(WebElement elemento) {
		Actions action = new Actions(driver);
		WebElement elemento1 = wait.until(ExpectedConditions.visibilityOf(elemento));
		action.moveToElement(elemento1).build().perform();
	}
	
	public void combinarTeclas(WebElement element,String pCombTeclas) {
		
		element.sendKeys(pCombTeclas);
		
	}
	public void escribir(By locator, String texto) {
		
		wait.until(ExpectedConditions.elementToBeClickable(locator)).sendKeys(texto);
	}
	
	public WebElement getElemento(By locator) {
		return driver.findElement(locator);
	}
	
	public List<WebElement> getElements(By locator){
		
		return driver.findElements(locator);
	}
	
	public String getTexto(By locator) {
	
		return wait.until(ExpectedConditions.visibilityOfElementLocated(locator)).getText();

	}
	
	public String getTextElement(WebElement elemento) {
		return elemento.getText();
	}
	public String getTitle() {
		return driver.getTitle();
		
	}
	public void ordenarLista(ArrayList list)
	{
		Collections.sort(list);
	}
	
	public ArrayList<String> getWindows(){
		ArrayList tabs = new ArrayList(driver.getWindowHandles());
		return tabs;
	}
	public String getWindow() {
		
		return driver.getWindowHandle();
	}
	
	public void changeWindow(String ventana) {
		driver.switchTo().window(ventana.toString());
	}
	
	public void changeIframe(int frame)
	{
		driver.switchTo().frame(frame);
	}
	
	public void changePageFrame() {
		driver.switchTo().defaultContent();
	}
	
	public String getValueAttribute(WebElement elemento, String atributo)
	{
		return elemento.getAttribute(atributo);
		
	}

	public void selectOption(WebElement element, String opcion) {
		Select select = new Select(element);
		select.selectByValue(opcion);
	}
	
	public boolean isDisplayed(By locator) {
		return driver.findElement(locator).isDisplayed();
	}
	
	
	public void takeScreenShot(String nombreImagen) {
		
		File evidencia = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
		try {	FileUtils.copyFile(evidencia, new File( directorioEvidencias +nombreImagen));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e);
		}
			
		
	}
}
