package pages;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.BackingStoreException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ListProductsPage extends CommonBase {

	// LOCATORS PAGE
	
	//localizadores para navegar por el menú de categorias
	
	private By categoryWomen = By.xpath("//a[@title='Women']");
	private By categoryDresses = By.xpath("//div/ul/li/a[@title='Dresses']");
	// li[@class='sfHoverForce']/ul/li/a[@title='Dresses']
	private By categoryTshirts = By.xpath("//div/ul[1]/li[3]/a[@title='T-shirts']");
	private By categoriesChildDresse = By.xpath("//div/ul/li[2]/ul/li/a");
	private By categoriesWomen = By.xpath("//li[1]/ul/li/a[@class='sf-with-ul']");
	private By categoriesChildWomen = By.xpath("//li[1]/ul[1][contains(@class,'submenu-container')]/li/ul/li/a");
	
	//localizadores para conocer los nombres y precios de productos
	private By nombresProductos = By.xpath("//div/div/h5/a[@class='product-name']");
	private By cantidadProductosCategoria = By.xpath("//span[@class='heading-counter']");
	private By precioProductosCategoria = By.xpath("//div[@class='right-block']/div[@class='content_price']/span[@class='price product-price']");
	
	//localizadores para ver vista miniatura de items desde el listado
	private By imagenesProductos = By.xpath("//div[@class='product-container']");
	private By btnVerVistaMiniatura = By.xpath("//a[@class='quick-view']");
	private By nombreItemVistaMiniatura = By.xpath("//h1[@itemprop='name']");
	private By btnCerrarVistaMiniatura = By.xpath("//a[@title='Close']");
	
	private By btnAdicionarItem = By.xpath("//a[@title='Add to cart']");
	private By msjRespuestaAdicionItem = By.xpath("//*[text()[contains(.,'Product successfully added to your shopping cart')]]");
	private By nombreItemVistaAdicionar = By.xpath("//span[@id='layer_cart_product_title']");
	private By btnCerrarVistaAdicionar = By.xpath("//span[@title='Close window']");
	private By btnContinuar = By.xpath("//*[@title='Continue shopping']");
	private By preciosItem = By.xpath("//div[@class='product-image-container']/div/span[@itemprop='price']");
	/**
	 * @param driver
	 * @param wait
	 */
	public ListProductsPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
		// TODO Auto-generated constructor stub
	}

	public void moverCursorCategoriaPrincipal(By locator) {

		mouseOver(locator);
	}

	public void abrirCatHijas(String teclas, By locator) {
		
		List<WebElement> lista = getElements(locator);
		
			for (WebElement elemento : lista) {
				combinarTeclas(elemento, teclas);
			}
	}
	
	public void  abrirCategoriaPrincipal(By locator, String teclas)
	{
		WebElement elemento = getElemento(locator);
		combinarTeclas(elemento, teclas);
		
	}
	
	public String darClicCategoria(By locator) {
		
		darClic(locator);
		return getTexto(cantidadProductosCategoria);
	}


	public ArrayList<String> navegarCategorias() {
		ArrayList<String> listaNavegacion = getWindows();

		String ventanaPrincipal = listaNavegacion.get(0);
		System.out.println("hay: " + listaNavegacion.size() + " ventanas abiertas");

		ArrayList<String> listaTitulosCategoria = new ArrayList<String>();
		int i = 0;
		while (i < listaNavegacion.size()) {

			String elemento = listaNavegacion.get(i++);
			changeWindow(elemento);
			System.out.println(getTitle());
			listaTitulosCategoria.add(getTitle());
		}
		changeWindow(ventanaPrincipal);
		return listaTitulosCategoria;

	}
	
	public ArrayList<String> verNombreProductoEnListado(By locator, String atributo) {
		
		ArrayList<String> resultado = new ArrayList<String>();
		List<WebElement> listaNombres = getElements(locator);
		for (WebElement elemento : listaNombres) {
			String nombreItem = getValueAttribute(elemento, atributo);
			System.out.println("El valor del atributo title es:"+nombreItem);
			resultado.add(nombreItem);
		}
		return resultado;		
	}
	
	public ArrayList<String> verPrecioProductoEnListado(By locator) {
		
		ArrayList<String> resultado = new ArrayList<String>();
		List<WebElement> listaPrecios = getElements(locator);
		for (WebElement elemento : listaPrecios) {
			String precioItem = getTextElement(elemento);
			System.out.println("El valor del precio es:"+ precioItem);
			resultado.add(precioItem);
		}
		return resultado;
	}
	
	public ArrayList<String> verVistaMiniaturaItem(By locatorImagenes, By locatorBtnVistaMiniatura, By locatorNombreItem, By locatorCerrarVistaMiniatura){
		
		List<WebElement> imagenes = getElements(locatorImagenes);
		List<WebElement> vistas = getElements(locatorBtnVistaMiniatura);
		ArrayList<String> resultado = new ArrayList<String>();
		String ventanaPrincipal = getWindow();
		int i =0;
		while (i<imagenes.size())
		{
				mouseOverElement(imagenes.get(i));
				mouseOverElement(vistas.get(i));
				darClicElement(vistas.get(i));
				changeIframe(0);
				String nombre = getTexto(locatorNombreItem);
				resultado.add(nombre);
				changeWindow(ventanaPrincipal);
				darClic(locatorCerrarVistaMiniatura);
				i++;
				//aplicar una espera,no se está esperando que desaparezca el popup
			
		}	return resultado;	
	}
	
	public ArrayList<String> agregarItem(String mensaje,List<String> lPrecios){
		
		List<WebElement> imagenes = getElements(imagenesProductos);
		List<WebElement> btnAdicionar = getElements(btnAdicionarItem);
		List<WebElement> precios = getElements(preciosItem);
		ArrayList<String> resultado = new ArrayList<String>();
		
		int i =0;
		while (i<imagenes.size()) {
			
			mouseOverElement(imagenes.get(i));
			mouseOverElement(btnAdicionar.get(i));
			String precioItem =getTextElement(precios.get(i));
			System.out.println("el precio es:"+precioItem);
			darClicElement(btnAdicionar.get(i));
					
			String texto = getTexto(msjRespuestaAdicionItem);
			
			if(texto.contains("Product successfully added") && (precioItem.equals(lPrecios.get(i))))
			{
				String item = getTexto(nombreItemVistaAdicionar);
				resultado.add(item);
				darClic(btnContinuar);
			}
			else {
				System.out.println("Ocurrió un error adicionando el item a la canasta.");
			}
			i++;
		}
		return resultado;
	}

	public By getCategoryWomen() {
		return categoryWomen;
	}

	public By getCategoryDresses() {
		return categoryDresses;
	}

	public By getCategoryTshirts() {
		return categoryTshirts;
	}

	public By getCategoryChildDresse() {
		return categoriesChildDresse;
	}

	public By getCategoriesdWomen() {
		return categoriesWomen;
	}

	public By getCategoriesChildWomen() {
		return categoriesChildWomen;
	}

	public By getNombresProductos() {
		return nombresProductos;
	}

	public By getPrecioProductosCategoria() {
		return precioProductosCategoria;
	}

	public By getImagenesProductos() {
		return imagenesProductos;
	}

	public By getBtnVerVistaMiniatura() {
		return btnVerVistaMiniatura;
	}

	public By getNombreItemVistaMiniatura() {
		return nombreItemVistaMiniatura;
	}

	public By getBtnCerrarVistaMiniatura() {
		return btnCerrarVistaMiniatura;
	}

	public By getBtnAdicionarItem() {
		return btnAdicionarItem;
	}

	public By getMsjRespuestaAdicionItem() {
		return msjRespuestaAdicionItem;
	}

	public By getBtnCerrarVistaAdicionar() {
		return btnCerrarVistaAdicionar;
	}

	

}
