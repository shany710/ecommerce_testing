package configuracion;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonDriver {

	WebDriver driver;
	String directorioChromeDriver;
	WebDriverWait espera;
	
	public CommonDriver(String navegador) {
		
		directorioChromeDriver = System.getProperty("user.dir") + "\\resources\\drivers\\chrome\\chromedriver.exe";
		
		System.out.println(directorioChromeDriver);
			
		if(navegador.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver",directorioChromeDriver);
			driver = new ChromeDriver();
		}
		
		espera = new WebDriverWait(driver, 10);
	}
	
	public void getUrl(String url) {
		
		driver.get(url);
	}
	public void maximizarNavegador() {
		
		driver.manage().window().maximize();
	}
	
	public void avanzar() {
		
		driver.navigate().forward();
	}
	
	public void retroceder() {
		
		driver.navigate().back();
	}
	
	public void quitBrowser() {
		driver.quit();
	}
	
	public String getDirectorioChromeDriver() {
		return directorioChromeDriver;
	}
	
	public WebDriver getDriver() {
		return driver;
	}

	public WebDriverWait getEspera() {
		return espera;
	}
	
}
